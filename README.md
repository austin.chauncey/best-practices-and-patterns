# POC Using GitLab for Best Practices & Patterns

Documenting our code-specific practices & patterns in version control gives us a few benefits

1. Historical view of decision making, we can always look back and see exactly why & when decisions were made. E.g. why we chose Nest.JS, TypeORM, colocating data depencies, using inputs for mutation arguments - etc. (No more searching one-off slack threads)
2. Everyone can easily request input on ways to improve our codebase & processes
3. We're all very used to README's and MR's as developers
4. (opinion) better format for these types of dicussions than notion (code in text editor, etc)
5. Can remove feeling of force-fed practices
6. Method used in open-source
7. Not one/small group of people making all decisions
8. No bouncing dicussions between notion/slack
9. Answers to questions that will probably be asked again
10. Can setup notifications when patterns & practices change to keep everyone informed

Can always sync to Notion as well from GitLab and lock pages if afraid of multiple places for documentation.
